Checkout and Run lpx
=====================

This repo contains a collection of libraries to run and process LPX DGVM using
linux-based commands and/or R functions. it is assumed they will be installed in
the 'libs/' directory of a project and called from the project root.

An example of how to incorporate these into project, run the following:

    git clone https://douglask3@bitbucket.org/douglask3/lpxcheckoutlibs.git libs/lpxcheckoutlibs/

LPX model
=========

Obtaining LPX
-------------
To clone and checkout LPX, run:

    ./checkoutLPX.sh <<xxxxxxx>>

This downloads the LPX model into a the directory 'lpx'

Configuring LPX
---------------
The `lpj.cfg` configuration file tells LPX where climate, land surface and CO2
inputs are stored, how long it will run through it various phases, and where to
output its spin up and final output files. This can be constructed using the
following R function:

    lpj.cfg(climFilePath = "", tempFile, tempMinFile, tempMaxFile,
            prepFile, wetdaysFile, windspeedFile,
            sunFile, sunIsCloud = TRUE,
            LgtnFilePath = climFilePath, LgtnFile,
            maskFilePath = climFilePath, maskFile, maskVar = "mask",
                maskType = "BOOLEAN",
            soilFilePath = maskFilePath, soilFile, soilVar = "soiltype",
            humnFilePath = maskFilePath,  popFile, aNdFile,
            agriFilePath = humnFilePath, cropFile, pasFile,
            nyrsSpinup = 10000, nyrsRampup = 0, myrsRun = 155,
            startYearsOutput = 100, frequancyRunup =1,
            CrbnFilePath = climFilePath,
                CO2File, CO2Var = co2,
                C13File, C13Var = c13,
                C14NorthFile, C14EquatorFile, C14SouthFile, C14Var = c14,
            SpinFilePath = outpFilePath, spinupFile = tempFile('Spinup'),
            rampupFile = tempFile('Rampup'),
            outputFilePath = "", outputFile,
            outputVars = c('fpc_grid', 'mfire_frac'))

I shall add more explanation about what these arguments set later. This
information is outputted to the file 'lpx/lpj.cfg' which will be read by LPX
when run.

Running LPX
-----------
LPX can be run it two ways: 1) one run or 2) with spinup done in one or more
phases and the main run done after. 1 is useful for equlibrium runs, 2 is
required for transient runs.

1. can be run from terminal as:

    ./motif-lpj

2. can be run from terminal as:
    ./motif-lpj-step1a  #runs spinup from scratch
    ./motif-lpj-step1b  #runs spinup again from end of previous spin up
    ./motif-lpj-step2   #runs main run from end of spin up.

Note that `lpj.cfg` will often need changing between 1a 1b and 2.

These can be run by:

    ./runLPX logA logB logC logD

where log is true or false. If A is true, the 1) is run; B step1a is run;
C step1b and; D step2


Installing the LPX output converter
-----------------------------------
The output converter converts the standard LPX outputs, which are in annual
netcdf files containing all variables set for output, into variable files
containing all timesteps. This conversion is useful for running most analysis
and when using the LPX benchmarking system. However, LPXs origonal output is
often better at archiving and sharing and they generally take up less disk space
and is more flexiable when sharing specific time periods. Information about how
the file converter works and how to intagrate it into your project can be found
[here]( https://douglask3@bitbucket.org/douglask3/lpxoutputconverter.git).

To clone the file converter, run:

    ./checkoutLPXOutConverter
