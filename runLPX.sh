#!/usr/bin/env sh
cd lpx
make clean
make all
if $1; then ./motif-lpj; fi
if $2; then ./motif-lpj-step1a; fi
if $3; then ./motif-lpj-step1b; fi
if $4; then ./motif-lpj-step2; fi

cd ..
