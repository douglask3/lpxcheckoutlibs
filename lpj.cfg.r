lpj.cfg <- function(climFilePath = "", tempFile, tempMinFile, tempMaxFile,
                    precFile, wetdaysFile, windspeedFile,
                    sunFile, sunIsCloud = TRUE,
                    LgtnFilePath = climFilePath, LgtnFile,
                    maskFilePath = climFilePath, maskFile, maskVar = "mask",
                        maskType = "BOOLEAN",
                    soilFilePath = maskFilePath, soilFile, soilVar = "soiltype",
                    humnFilePath = maskFilePath,  popFile, aNdFile,
                    agriFilePath = humnFilePath, cropFile, pasFile,
                    CrbnFilePath = climFilePath,
                        CO2File, CO2Var = 'co2',
                        C13File, C13Var = 'c13',
                        C14NorthFile, C14EquatorFile, C14SouthFile, C14Var = 'c14',
                    nyrsSpinup = 10000, nyrsRampup = 0, myrsRun = 155,
                    startYearsOutput = 100, frequancyRunup =1,
                    spinFilePath = outputFilePath, spinupFile = tempFile('Spinup'),
                    rampupFile = tempFile('Rampup'),
                    outputFilePath = "", outputFile,
                    outputVars = c('fpc_grid', 'mfire_frac'),
                    includeProjectVersionNumber = TRUE) {

if ( 'gitProjectExtras' %in% rownames(installed.packages()) == FALSE)
    install_git('https://bitbucket.org/douglask3/gitprojectextras/admin',
        'gitProjectExtras')
library('gitProjectExtras')
convertRlog2CommandLog <- function(logical)
    if (logical) return('true') else return('false')


gitInfo = c("lpxVer-"    ,  gitVersionNumber(gitLoc = 'lpx/.git'),
            "_prjctVer-" ,  gitVersionNumber())


if (!includeProjectVersionNumber) gitInfo = head(gitInfo,-1)
gitInfo = paste(gitInfo, collapse ='')

addCD <- function(x) paste('../', x, sep = "")

spinFilePath   = addCD(spinFilePath)
outputFilePath = addCD(outputFilePath)

SPINUP_FILE = paste(spinFilePath  , spinupFile, nyrsSpinup             , gitInfo, ".txt", sep = "-")
RAMPUP_FILE = paste(spinFilePath  , rampupFile, nyrsSpinup + nyrsRampup, gitInfo, ".txt", sep = "-")
OUTPUT_FILE = paste(outputFilePath, outputFile                         , gitInfo        , sep = "-")

sunIsCloud = convertRlog2CommandLog(sunIsCloud)
outputVars = paste("\nOUTPUT_VAR:", outputVars)

cat("
TEMP: ", climFilePath, tempFile, "
TMIN: ", climFilePath, tempMinFile, "
TMAX: ", climFilePath, tempMaxFile, "
PREC: ", climFilePath, precFile, "
WETDAYS: ", climFilePath, wetdaysFile, "
WINDSPEED: ", climFilePath, windspeedFile, "
SUN: ", climFilePath, sunFile, "
SUN_IS_CLOUD: ", sunIsCloud, "
LIGHTN: ", LgtnFilePath, LgtnFile, "
MASK: ", maskFilePath, maskFile, "
MASK_VAR: ", maskVar, "
MASK_TYPE: ", maskType, "
SOIL_TYPE: ", soilFilePath, soilFile, "
SOIL_TYPE_VAR: ", soilVar, "
POPDENS: ", humnFilePath, popFile, "
A_ND: ", humnFilePath, aNdFile, "
CROP: ", agriFilePath, cropFile, "
PAS: ", agriFilePath, pasFile, "
CO2_FILE: ", CrbnFilePath, CO2File, "
C13_FILE: ", CrbnFilePath, C13File, "
C14_NORTH_FILE: ", CrbnFilePath, C14NorthFile, "
C14_EQUATOR_FILE: ", CrbnFilePath, C14EquatorFile, "
C14_SOUTH_FILE: ", CrbnFilePath, C14SouthFile, "
CO2_VAR: ", CO2Var, "
C13_VAR: ", C13Var, "
C14_VAR: ", C14Var, "
SPINUP_YEARS: ", nyrsSpinup, "
RAMPUP_YEARS: ", nyrsRampup, "
RUN_YEARS: ", nyrsRun, "
RUN_OUT_YEARS: ", startYearsOutput, "
RUN_OUT_FREQ: ", frequancyRunup, "
SPINUP_FILE: ", SPINUP_FILE, "
RAMPUP_FILE: ", RAMPUP_FILE, "
OUTPUT_FILE: ", OUTPUT_FILE,
outputVars, "\n", file = 'lpx/lpj.cfg', sep = '')

removeCD <- function(x) strsplit(x,'../',fixed = TRUE)[[1]][2]

SPINUP_FILE = removeCD(SPINUP_FILE)
RAMPUP_FILE = removeCD(RAMPUP_FILE)
OUTPUT_FILE = removeCD(OUTPUT_FILE)

OUTPUT_FILE = paste(OUTPUT_FILE, '-', nyrsSpinup + nyrsRampup +
                    seq(startYearsOutput, nyrsRun, by = frequancyRunup), '.nc',
                    sep ='')

return(list(SPINUP_FILE, RAMPUP_FILE, OUTPUT_FILE))
}
