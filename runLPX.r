runLPX <- function(oneStep = FALSE, step1a = FALSE, step1b = FALSE, step2 = FALSE,
                   pipeFile = NULL, ...) {

    convertRlog2CommandLog <- function(logical)
        if (logical) return('true') else return('false')

    oneStep = convertRlog2CommandLog(oneStep)
    step1a  = convertRlog2CommandLog(step1a )
    step1b  = convertRlog2CommandLog(step1b )
    step2   = convertRlog2CommandLog(step2  )

    command = './libs/lpxcheckoutlibs/runLPX.sh'
    command = paste(command, oneStep, step1a, step1b, step2)
    if(!is.null(pipeFile)) command = paste(command, '>', pipeFile)

    system(command, ...)
}
