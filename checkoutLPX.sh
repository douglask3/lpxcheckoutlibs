#!/usr/bin/env sh
echo "checking out lpx"
./libs/lpxcheckoutlibs/gitCloneRepo.sh https://douglask3@bitbucket.org/douglask3/lpd.git "lpx"

cd lpx
echo $1
git checkout -f $1
make clean
cd ..
